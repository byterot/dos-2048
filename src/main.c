#include <sys/nearptr.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "base/vga.h"
#include "base/inp.h"
#include "base/pcx.h"

#include <dos.h>

struct PcxImage pcx_from_file(char const *filename)
{
  FILE *fp = fopen(filename, "rb");
  struct PcxImage image = pcx_load(fp);
  fclose(fp);

  return image;
}

//int const grid_size = 4;
#define grid_size 4
int grid[grid_size][grid_size];
int const tile_w = 40;
int const tile_h = 40;
int const grid_x = 160 - (grid_size / 2) * tile_w;
int const grid_y = 100 - (grid_size / 2) * tile_h;

struct PcxImage frame;
struct PcxImage numbers[11];

void draw_grid()
{
  for(int y = 0; y < grid_size; ++y)
    for(int x = 0; x < grid_size; ++x)
    {
      pcx_draw(&frame, 
          grid_x + (x * tile_w),
          grid_y + (y * tile_h));

      int grid_val = grid[y][x];
      if(grid_val > 0)
      {
        struct PcxImage* image = &numbers[grid_val - 1];

        pcx_draw(image, 
            grid_x + (x * tile_w),
            grid_y + (y * tile_h));
      }

    }
}

void add_new_random_tile(int tile_value)
{
  int board_full = 1;
  for(int y = 0; y < grid_size; ++y)
    for(int x = 0; x < grid_size; ++x)
      if(grid[y][x] == 0)
      {
        board_full = 0;
        break;
      }

  if(board_full) 
  {
    return;
  }

  int i = rand() % grid_size * grid_size * 2;

  while(i >= 0)
  {
    for(int y = 0; y < grid_size; ++y)
      for(int x = 0; x < grid_size; ++x)
        if(grid[y][x] == 0)
        {
          if(i > 0) 
          {
            --i;
            continue;
          }

          grid[y][x] = tile_value;

          return;
        }
  }
}

int check_win_condition()
{
  for(int y = 0; y < grid_size; ++y)
    for(int x = 0; x < grid_size; ++x)
      if(grid[y][x] == 11)
      {
        return 1;
      }

  return 0;
}

int check_fail_condition()
{
  for(int y = 0; y < grid_size; ++y)
    for(int x = 0; x < grid_size; ++x)
    {
      int value = grid[y][x];
      if(value == 0) return 0;

      if(y > 0 && 
          (grid[y - 1][x] == value ||
          grid[y - 1][x] == 0)) return 0;
      if(y < grid_size - 1 &&
          (grid[y + 1][x] == value ||
          grid[y + 1][x] == 0)) return 0;

      if(x > 0 &&
          (grid[y][x - 1] == value ||
          grid[y][x - 1] == 0)) return 0;
      if(x < grid_size -1 &&
          (grid[y][x + 1] == value ||
          grid[y][x + 1] == 0)) return 0;
    }

  return 1;
}

int move_grid_up()
{
  int moved = 0;
  int locked[grid_size][grid_size];

  for(int y = 0; y < grid_size; ++y)
    for(int x = 0; x < grid_size; ++x)
    {
      locked[y][x] = 0;
    }

  for(int y = 1; y < grid_size; ++y)
    for(int x = 0; x < grid_size; ++x)
    {
      if(grid[y][x] == 0) continue;

      int value = grid[y][x];
      int target_y = y;

      for(int y2 = y - 1; y2 >= 0; --y2)
      {
        if(locked[y2][x] != 0) break;

        if(grid[y2][x] == 0)
        {
          target_y = y2;
        } 
        else if(grid[y2][x] == value)
        {
          target_y = y2;
          break;
        }
        else break;
      }

      if(target_y == y) continue;

      if(grid[target_y][x] == 0)
      {
        grid[target_y][x] = value;
        grid[y][x] = 0;
        moved = 1;
      }
      else if(grid[target_y][x] == value)
      {
        ++grid[target_y][x];
        locked[target_y][x] = 1;
        grid[y][x] = 0;
        moved = 1;
      }
    }

  return moved;
}

int move_grid_down()
{
  int moved = 0;
  int locked[grid_size][grid_size];

  for(int y = 0; y < grid_size; ++y)
    for(int x = 0; x < grid_size; ++x)
    {
      locked[y][x] = 0;
    }

  for(int y = grid_size - 1; y >= 0; --y)
    for(int x = 0; x < grid_size; ++x)
    {
      if(grid[y][x] == 0) continue;

      int value = grid[y][x];
      int target_y = y;

      for(int y2 = y + 1; y2 < grid_size; ++y2)
      {
        if(locked[y2][x] != 0) break;

        if(grid[y2][x] == 0)
        {
          target_y = y2;
        } 
        else if(grid[y2][x] == value)
        {
          target_y = y2;
          break;
        }
        else break;
      }

      if(target_y == y) continue;

      if(grid[target_y][x] == 0)
      {
        grid[target_y][x] = value;
        grid[y][x] = 0;
        moved = 1;
      }
      else if(grid[target_y][x] == value)
      {
        ++grid[target_y][x];
        locked[target_y][x] = 1;
        grid[y][x] = 0;
        moved = 1;
      }
    }

  return moved;
}

int move_grid_right()
{
  int moved = 0;
  int locked[grid_size][grid_size];

  for(int y = 0; y < grid_size; ++y)
    for(int x = 0; x < grid_size; ++x)
    {
      locked[y][x] = 0;
    }

  for(int y = 0; y < grid_size; ++y)
    for(int x = grid_size - 1; x >= 0; --x)
    {
      if(grid[y][x] == 0) continue;

      int value = grid[y][x];
      int target_x = x;

      for(int x2 = x + 1; x2 < grid_size; ++x2)
      {
        if(locked[y][x2] != 0) break;

        if(grid[y][x2] == 0)
        {
          target_x = x2;
        } 
        else if(grid[y][x2] == value)
        {
          target_x = x2;
          break;
        }
        else break;
      }

      if(target_x == x) continue;

      if(grid[y][target_x] == 0)
      {
        grid[y][target_x] = value;
        grid[y][x] = 0;
        moved = 1;
      }
      else if(grid[y][target_x] == value)
      {
        ++grid[y][target_x];
        locked[y][target_x] = 1;
        grid[y][x] = 0;
        moved = 1;
      }
    }

  return moved;
}

int move_grid_left()
{
  int moved = 0;
  int locked[grid_size][grid_size];

  for(int y = 0; y < grid_size; ++y)
    for(int x = 0; x < grid_size; ++x)
    {
      locked[y][x] = 0;
    }

  for(int y = 0; y < grid_size; ++y)
    for(int x = 1; x < grid_size; ++x)
    {
      if(grid[y][x] == 0) continue;

      int value = grid[y][x];
      int target_x = x;

      for(int x2 = x - 1; x2 >= 0; --x2)
      {
        if(locked[y][x2] != 0) break;

        if(grid[y][x2] == 0)
        {
          target_x = x2;
        } 
        else if(grid[y][x2] == value)
        {
          target_x = x2;
          break;
        }
        else break;
      }

      if(target_x == x) continue;

      if(grid[y][target_x] == 0)
      {
        grid[y][target_x] = value;
        grid[y][x] = 0;
        moved = 1;
      }
      else if(grid[y][target_x] == value)
      {
        ++grid[y][target_x];
        locked[y][target_x] = 1;
        grid[y][x] = 0;
        moved = 1;
      }
    }

  return moved;
}

int main()
{
  if(!__djgpp_nearptr_enable())
  {
    printf("Could not enable nearptr\n");
    return 1;
  }

  vga_setmode(0x13);

  vga_clear(0);
  keyboard_bind();

  // Load pcx images
  frame = pcx_from_file("frame.pcx");
  for(int i = 0; i < 11; ++i)
  {
    int nr = 2 << i;

    char str[32];
    sprintf(str, "%d.pcx", nr);

    numbers[i] = pcx_from_file(str);
  }

  // Clear grid
  for(int y = 0; y < grid_size; ++y)
    for(int x = 0; x < grid_size; ++x)
    {
      grid[y][x] = 0;
    }

  srand(time(NULL));

  add_new_random_tile(1);
  vga_clear(0);
  draw_grid();

  // Wait for input
  while(1)
  {
    keyboard_fetch();
    if(key_pressed(0x1)) break;

    int redraw = 0;

    if(key_pressed(0x11)) // up
    {
      redraw = move_grid_up();
    }
    else if(key_pressed(0x1F)) // down
    {
      redraw = move_grid_down();
    }
    else if(key_pressed(0x20)) // right
    {
      redraw = move_grid_right();
    }
    else if(key_pressed(0x1E)) // right
    {
      redraw = move_grid_left();
    }

    if(check_win_condition())
    {
      draw_grid();
      printf("You win! :D\n");
      delay(5000);
      break;
    }

    if(check_fail_condition())
    {
      draw_grid();
      printf("No more moves possible. You lost :(\n");
      delay(5000);
      break;
    }

    if(redraw)
    {
      add_new_random_tile(1);
      //vga_clear(0);
      draw_grid();
    }

    delay(1); 
  }

  pcx_unload(&frame);
  keyboard_unbind();

  vga_setmode(0x3);

  __djgpp_nearptr_disable();

  printf("Thank you for playing dos-2048\n\n");
  return 0;
}
