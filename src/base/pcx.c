#include "pcx.h"
#include "vga.h"

#include <stdlib.h>

PcxImage pcx_load(FILE *fp)
{
  PcxImage image;
  image.width = 0;
  image.height = 0;

  fseek(fp, 4, SEEK_SET);

  int minx, miny, maxx, maxy;

	minx = fgetc(fp);
	minx = minx | fgetc(fp) << 8;
	miny = fgetc(fp);
	miny = miny | fgetc(fp) << 8;
	maxx = fgetc(fp);
	maxx = maxx | fgetc(fp) << 8;
	maxy = fgetc(fp);
	maxy = maxy | fgetc(fp) << 8;

  image.width = maxx - minx + 1;
  image.height = maxy - miny + 1;

  image.data = (char*)malloc(image.width * image.height);

  fseek(fp, 128, SEEK_SET);

	int i = 0;
	char b, c;
	do {
		b = fgetc(fp);

		if(b & 0xC0) { // check for RLE
			b = b & 0x3F; // remove RLE signature to get run length
			c = fgetc(fp); // get color

			for(char j = 0; j < b; ++j) {
				image.data[i] = c;
				++i;
			}
		} else {
			image.data[i] = b;
			++i;
		}
	} while(i < image.width * image.height);

  return image;
}


void pcx_unload(PcxImage *image)
{
  free(image->data);
}

void pcx_draw(PcxImage *image, int x, int y)
{
  int endx = x + image->width;
  if(endx <= 0) return;
  if(endx >= 320) endx = 319;

  int endy = y + image->height;
  if(endy <= 0) return;
  if(endy >= 200) endy = 199;

  int tx = 0, ty = 0;
  while(y < endy)
  {
    while(x < endx)
    {
      char color = image->data[ty * image->width + tx];

      if(color != 0)
      {
        vga_pixel(x, y, color); 
      }

      ++x;
      ++tx;
    }

    x -= image->width;
    tx = 0;

    ++y;
    ++ty;
  }
}
