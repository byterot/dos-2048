#pragma once

#include <stdio.h>

struct PcxImage
{
  char *data;
  int width, height;
};

PcxImage pcx_load(FILE *fp);
void pcx_unload(PcxImage *image);

void pcx_draw(PcxImage *image, int x, int y);
