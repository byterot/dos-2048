#include "vga.h"
#include "dpmi.h"   // for interrupts
#include "pc.h"     // for ports
#include <sys/nearptr.h>

#define VGASTART 0xA0000

void vga_setmode(int mode)
{
    __dpmi_regs r;
    r.x.ax = mode;
    __dpmi_int(0x10, &r);
}

void vga_setcolor(int color,  unsigned char r, unsigned char g, unsigned char b)
{
    // VGA DAC address register: 0x3C8
    // VGA DAC data register: 0x3C9
    outportb(0x3C8, color);
    outportb(0x3C9, r);
    outportb(0x3C9, g);
    outportb(0x3C9, b);
}

void vga_sync()
{
    // wait for current retrace, if any
    //do{}
    //while(inportb(0x3DA) & 8);

    //wait for new retrace
    do{}
    while(!(inportb(0x3DA) & 8));
}

void vga_clear(unsigned char color)
{
    int l = 320 * 200 / 4;
    unsigned volatile int *v = vgaptr32();
    int c = color << 24 | color << 16 | color << 8 | color; 
    for(int i = 0; i < l; ++i)
    {
        *v = c;
        ++v;
    }
}

unsigned volatile char *vgaptr()
{
    return (unsigned volatile char *)(VGASTART + __djgpp_conventional_base);
}

unsigned volatile short *vgaptr16()
{
    return (unsigned volatile short *)(VGASTART + __djgpp_conventional_base);
}

unsigned volatile int *vgaptr32()
{
    return (unsigned volatile int *)(VGASTART + __djgpp_conventional_base);
}

void vga_pixel(int x, int y, char color)
{
  *(vgaptr() + (y * 320) + x) = color;
}
